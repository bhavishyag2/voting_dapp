/*
Create a dapp for voting where all of the votes and candidate registration happens on chain. 
Allow anyone to start an election with a registration period, voting period, and ending time. 
Allow anyone to sign up as a candidate during the registration period, and allow anyone to vote once during the voting period. 
Create a front end where voters can see the results and know how long is left in the election.
*/



pragma solidity ^0.8.4;

contract voting{
    
    address[] candidates;
    
    mapping(address=>bool) public voteGiven;
    mapping(address=>int) public voteCount;
    
    uint startTime;
    uint registrationPeriod;
    uint votingPeriod;
    uint endingTime;
    
    address owner;
    constructor() public{
        owner=msg.sender;
        startTime=uint(block.timestamp);
        registrationPeriod= startTime + 10 days;
        votingPeriod = registrationPeriod + 10 days;
    }
    
    function candidateRegister() public {
        require(uint(block.timestamp) < registrationPeriod);
        candidates.push(msg.sender);
        voteCount[msg.sender]=0;
    }
    
    function giveVote(address x) public{
        require(uint(block.timestamp) > registrationPeriod);
        require(uint(block.timestamp) < votingPeriod);
        require(voteGiven[msg.sender]==false);
        voteGiven[msg.sender]=true;
        voteCount[x]++;
    }
    
    
    function Result() public view returns(address){
        require(msg.sender==owner);
        require(uint(block.timestamp)> votingPeriod);
        int max=-1;
        address fin;
        for(uint i=0;i<candidates.length;i++){
            if(voteCount[candidates[i]] > max){
                max=voteCount[candidates[i]];
                fin=candidates[i];
            }
        }
        return fin;
    }
    
    
    
    
    
    
    
}